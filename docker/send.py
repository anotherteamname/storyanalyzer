"""
Author: Jacob Vines
Date: 2/11/2019

Purpose: Simple client to demonstrate the operation of the server.
"""

import requests

#http://docs.python-requests.org/en/latest/user/quickstart/#post-a-multipart-encoded-file

url = "http://localhost:8000/"  # may need to change port number, depending on exposed docker port
fin = open('testout.csv', 'rb')
files = {'file': fin}
try:
    r = requests.post(url, files=files)
    print(r.text)
except Exception as e:
    print(e)
finally:
    fin.close()
    
try:
    print(url + 'testout.csv')
    d = requests.get(url + 'testout.csv')
    print(d.text)
except Exception as e:
    print(e)
