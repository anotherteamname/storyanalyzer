"""
Author: Jacob Vines
Date: 2/2/2019

Purpose: Simple script to demonstrate containerization 
of python app using nltk.
"""

from nltk.corpus import brown
import csv
''' 
	From the brown corpus, pull into bag of words
'''
bow = brown.words()

with open("./output/test.csv", 'w') as outfile:
	writer = csv.writer(outfile,quoting=csv.QUOTE_ALL)
	writer.writerow(bow)

for word in bow:
	print(word,end =" ")

print()
print("Ha. It works.")
print("I mean, I'm surprised, honestly")

