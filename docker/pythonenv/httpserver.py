"""
Author: Jacob Vines
Date: 2/11/2019

Purpose: Simple HTTPServer for handling post requests. Takes a file, adds
         a row, returns that file back to client.
"""

# http://flask.pocoo.org/docs/patterns/fileuploads/
import os
from Placeholder_Demo import story_length
import csv
from flask import Flask, request, redirect, url_for, send_from_directory, send_file
from werkzeug import secure_filename
import sys
import logging


UPLOAD_FOLDER = 'output'
ALLOWED_EXTENSIONS = set(['csv'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

log = Flask.logger


user_stories=[['Words','More Words'],['Even More Words','Some words']]




def allowed_file(filename):
  # this has changed from the original example because the original did not work for me
    return filename[-3:].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        print("POST RECEIVED", file=sys.stderr)
        if file and allowed_file(file.filename):
            print('**found file', file.filename)
            filename = secure_filename(file.filename)
            abspath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(abspath)
            add_row(abspath)
            return url_for('uploaded_file',filename=filename)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>FAILURE.</h1>
    </html>
    '''
@app.route('/<filename>',methods=['GET'])
def get_file(filename):
    if request.method == 'GET':
        print("GET RECEIVED")
        abspath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        if os.path.isfile(abspath) and allowed_file(abspath):
            return send_file(abspath)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>FAILURE.</h1>
    </html>
    '''
    
    story_length(user_stories)


@app.route('/output/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

def add_row(absolute_filename):
    row = ["This","Is","A","New","Row"]
    with open(absolute_filename, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(row)
    return

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=80)
    while running:
        server.handle_request()


                   

