"""
Author: Aaron Farris, Jacob Vines
Date: 4/1/2019

Purpose: Holds class definition for the Perceptron
"""


import numpy as np

class Perceptron:
    def __init__(self, n_inputs, learning_rate=0.001, iterations=10, buildFromFile=True):
        self.n_inputs = n_inputs
        self.learning_rate = learning_rate
        self.iterations = iterations
        self.w_ = [0] * (n_inputs + 1)    #initialize weights vector to 0 (+1 for bias weight)
        self.w_[0] = 1
        self.errors_ = [0] * (iterations)
        if buildFromFile:
            self.buildWeightsFromFile()

    # TODO: Finalize training strategy --> 
    #   --> Experimentally identify a sufficient learning rate and iteration in init
    def train(self, inputvector, expected):
        """
            Fits the training data to the Perceptron
        """
        for i in range(self.iterations):
            for x in range(len(inputvector)):
                act = self.activate(inputvector[x])
                #print("Prediction: " + str(act))
                #print("Expected: " + str(expected[x]))
                error = (expected[x] - act) * self.learning_rate
                #print("Error = Expected - Act * learning rate(" + str(self.learning_rate) + "): " + str(error))
                if error != 0:
                    for n in range(1,len(self.w_)):
                        self.w_[n] = self.w_[n] + (error * inputvector[x][n-1])
                    self.w_[0] += error
                    self.errors_[i] += 1
                print("Trained on story " + str(x) + " of " + str(len(inputvector)) + ". Iteration " + str(i+1) + " of " + str(self.iterations) + ".")
        return

    def buildWeightsFromFile(self, file="nnweights.npy"):
        self.w_ = np.load(file)
        return

    def saveWeightsToFile(self, file="nnweights",ascsv=False):
        np.save(file,self.w_)
        if ascsv:
            np.savetxt(file + ".csv",self.w_)
        return

    def net_input(self, inputvector):
        """ Calculate the Net Input """
        dp = np.dot(inputvector, self.w_[1:])
        dp += self.w_[0]
        return dp

    def activate(self, inputvector):
        """ ReLU Activation Function: Returns the story point prediction. """
        netin = self.net_input(inputvector)
        if netin <= 1:
            return 1
        elif netin > 25:
            return 25
        else:
            return round(netin)

