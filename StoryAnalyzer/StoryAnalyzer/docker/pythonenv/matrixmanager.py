"""
Author: Jacob Vines
Date: 3/19/2019

Purpose: Sparse Matrix class to handle matrix mapping.
"""

import csv
import collections
from nltk.tokenize import RegexpTokenizer
from fileparser import getVectorFromFile

class Sparsematrix:
    dictionary = dict()
    populated = False
    regtokenizer = RegexpTokenizer(r'\w+')
    '''
        If autopopulate is set, then automatically load from path.

        If not set, populateFromFile must be called before mapping.
    '''
    def __init__(self, path='matrixdictionary.csv',autopopulate=True):
        if autopopulate:
            self.populateFromFile(path)
        return

    '''
        Populates local dictionary object with WORD COLUMN VECTOR stored in
        filename.
    '''
    def populateFromFile(self,filename):
        dictionary = getVectorFromFile(filename,columnsToInclude=1)
        for row in dictionary:
            self.dictionary[row] = 0
        self.populated = True
        return

    '''
        Note: userstories is a 1d list of user stories which have been 
        stopword processed, lemmatized/stemmed in the EXACT SAME sequence
        as the dataset which formed the sparse matrix dictionary.
    '''
    def map(self,userstory):  
        
        if not self.populated:
            return []

        orderedItems = []

        for word in userstory.split():
            if word in self.dictionary.keys():
                self.dictionary[word] += 1

        for key in sorted(self.dictionary.keys()):
            orderedItems.append(self.dictionary[key])

        for key in self.dictionary.keys():
            self.dictionary[key] = 0

        return orderedItems
        
    '''
        Note: dataset is a 1d list of user stories. Dataset should be
        stopword processed, lemmatized/stemmed. This exact dataset will be
        the input to the neural net.

        ANY calls to this method will require retraining the neural net.
    '''
    @staticmethod
    def datasetToDictionaryFile(dataset, savepath='matrixdictionary.csv'):
        regtokenizer = RegexpTokenizer(r'\w+')
        # First, migrate each word into a set
        dictionarySet = set()
        for story in dataset:
            for word in regtokenizer.tokenize(story):
                if not word.isdigit() and word.isalpha() and len(word) > 2 and len(word) < 21:
                    dictionarySet.add(word.lower())

        # Then, write to csv
        with open(savepath,'w',encoding="utf-8",newline='') as outputfile:
            csv_writer = csv.writer(outputfile)
            for word in dictionarySet:
                csv_writer.writerow([word])

        return