from fileparser import getVectorFromFile
from matrixmanager import Sparsematrix as sm
from nlpmanager import stop_and_stem
from techfactorsmanager import Techfactors

print("Getting vector from training set...")
vector = getVectorFromFile('./testing_sets/Training.csv')
print("Stopping vector...")
vector2 = stop_and_stem(vector)
print("Writing vector to file...")
sm.datasetToDictionaryFile(vector2)
print("Written to pythonenv/matrixdictionary.csv")