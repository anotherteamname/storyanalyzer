"""
Author: Chase Hileman, Jacob Vines
Date: 3/3/2019

Purpose: Function definition for stop word removal.
"""

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import RegexpTokenizer


'''
    user_stories is 1d list, each line a story 
'''
def remove_stopwords(user_stories):
    regtokenizer = RegexpTokenizer(r'\w+')
    stpwords = stopwords.words('english')
    return_words = []

    for row in user_stories:
        tokens = regtokenizer.tokenize(row)
        words = [word.lower() for word in tokens if not word in stpwords]
        return_words.append(' '.join(words))
    return return_words

'''
    userstory_vectors is a 1d list, each line a story
'''
def stem(user_stories):
    stemmer = PorterStemmer()
    regtokenizer = RegexpTokenizer(r'\w+')

    stemmed_stories = []

    for row in user_stories:
        stemmedrow = []
        tokens = regtokenizer.tokenize(row)
        for word in tokens:
            stemmedrow.append(stemmer.stem(word))
        stemmed_stories.append(' '.join(stemmedrow))
        
    return stemmed_stories

'''
    user_stories is a 1d list, each line a story
'''
def stop_and_stem(user_stories):
    stopped = remove_stopwords(user_stories)
    filtered_stories = stem(stopped)

    return filtered_stories