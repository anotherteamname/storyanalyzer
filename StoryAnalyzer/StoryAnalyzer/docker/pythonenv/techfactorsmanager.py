"""
Author: Jacob Vines
Date: 3/25/2019

Purpose: Tech Factors management
"""

from fileparser import getVectorFromFile
import os

class Techfactors:
    dictionary = dict()
    populated = False

    def __init__(self):
        # First, see if file "techfactors.csv" in input folder
        # If so, populate vector from that file (as it is a user supplied file)
        userpath = "./input/techfactors.csv"
        defaultpath = "techfactors.csv"
        tf = []
        if os.path.isfile(userpath):
            tf = getVectorFromFile(userpath,columnsToInclude=2)
            # delete user uploaded techfactors to ensure this process is repeatable
            os.remove(userpath)
        else:
            tf = getVectorFromFile(defaultpath,columnsToInclude=2)
        self.populateDictionary(tf)
        return

    '''
        Populates local dictionary object with tech factors and values.
        techfactors is 2d array such that array[i][0] = word, array[i][1] = value
    '''
    def populateDictionary(self,techfactors):
        for row in techfactors:
            self.dictionary[row[0]] = row[1]
        self.populated = True
        return

    '''
        Note: userstory is a user story which has been 
        stopword processed, lemmatized/stemmed, just before entry into NN.
    '''
    def calculate(self,userstory):
        if not self.populated:
            return 1
        tf = 1
        for row in userstory:
            words = row.split()
            for word in words:
                if word in self.dictionary:
                    tf *= float(self.dictionary[word])  #Assuming techfactor is a mere multiplication of all techfactor values
        return tf