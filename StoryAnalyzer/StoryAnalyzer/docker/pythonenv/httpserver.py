"""
Author: Jacob Vines
Date: 2/11/2019

Purpose: Simple HTTPServer for handling post requests. 
        Three URLs -- /train -> accepts a file to retrain NN.
                      /predict  -> accepts a file to predict
                      /retrieve/<filename> -> retrieve file in DOWNLOAD_FOLDER with filename
                      /factors -> accepts an uploaded techfactors csv, forced with a known filename

        Processes for prediction:
                      -> predict with default techfactors:
                            -> /predict upload file to evaluate
                            -> /retrieve/<filename> file with same filename as uploaded file
                      -> predict with custom techfactors:
                            -> /factors upload techfactors to use
                            -> /predict upload file to evaluate
                            -> /retrieve/<filename> file with same filename as uploaded file
        Notes for future devs:
              For techfactors: 
                    - The tech factors implementation should look first for file "techfactors.csv" in 
                    the upload folder, which can be received via parameter from the server file, here.
                    If no file is there, then use default in main directory rather than Upload. 
                    This will ensure that an uploaded techfactors file is prioritized above the default.
                    - After an uploaded tech factors file has been used, delete it. This will allow 
                    subsequent predictions to follow the same method above.
              For retraining:
                    - Remember that training dataset must also be passed through the pipe and filter
                    stage to remove stopwords and lemmatize before retraining.
              For prediction:
                    - Remember that the amount of time to produce predictions may be a few seconds. Client side
                    code should be written to repeatedly poll via the /retrieve url and catch any HTTP 404 errors returned
                    from a file which is not yet written to the download folder.

"""

# http://flask.pocoo.org/docs/patterns/fileuploads/
import os
import csv
from flask import Flask, request, redirect, url_for, send_from_directory, send_file
from werkzeug import secure_filename
import sys
import logging
from fileparser import getVectorFromFile, addValueColumn
from neuralnet import Perceptron
from techfactorsmanager import Techfactors
from matrixmanager import Sparsematrix
from nlpmanager import stop_and_stem

UPLOAD_FOLDER = 'input'
DOWNLOAD_FOLDER = 'output'
ALLOWED_EXTENSIONS = set(['csv'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER

log = Flask.logger

tf = Techfactors()
sm = Sparsematrix()
neuralnet = Perceptron(len(tf.dictionary), learning_rate=0.01, iterations=3)
neuralnet.buildWeightsFromFile()

def allowed_file(filename):
  # this has changed from the original example because the original did not
  # work for me
    return filename[-3:].lower() in ALLOWED_EXTENSIONS

@app.route('/train', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        print("POST RECEIVED", file=sys.stderr)
        if file and allowed_file(file.filename):
            print('**found file', file.filename)
            filename = secure_filename(file.filename)
            abspath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(abspath)
            train(abspath)
            return url_for('upload_file',filename=filename)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>FAILURE.</h1>
    </html>
    '''

@app.route('/retrieve/<filename>',methods=['GET'])
def get_file(filename):
    if request.method == 'GET':
        print("GET RECEIVED")
        abspath = os.path.join(app.config['DOWNLOAD_FOLDER'], filename)
        if os.path.isfile(abspath) and allowed_file(abspath):
            return send_file(abspath)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>FAILURE.</h1>
    </html>
    '''

@app.route('/factors',methods=['POST'])
def upload_tech_factors():
    forced_name = "techfactors.csv"
    if request.method == 'POST':
        file = request.files['file']
        print("POST RECEIVED", file=sys.stderr)
        if file and allowed_file(file.filename):
            print('**found file', file.filename)
            filename = secure_filename(forced_name) #Ensures that techfactors file is stored at a known path
            abspath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(abspath)
            return url_for('upload_tech_factors',filename=filename)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>FAILURE.</h1>
    </html>
    '''

@app.route('/predict',methods=['POST'])
def upload_predict_file():
    if request.method == 'POST':
        file = request.files['file']
        print("POST RECEIVED", file=sys.stderr)
        if file and allowed_file(file.filename):
            print('**found file', file.filename)
            filename = secure_filename(file.filename)
            abspath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            outputpath = os.path.join(app.config['DOWNLOAD_FOLDER'], filename)
            file.save(abspath)
            predict(abspath,outputpath)
            return url_for('upload_predict_file',filename=filename)
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>FAILURE.</h1>
    </html>
    '''

@app.route('/shutdown',methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running on Werkzeug Server')
    func()
    return

def predict(absolute_path,output_path):
    stories = getVectorFromFile(absolute_path)
    filtered = stop_and_stem(stories)
    predictions = []
    for story in filtered:
        factor = tf.calculate(story)
        storyMatrix = sm.map(story)
        prediction = neuralnet.activate(storyMatrix)
        prediction = prediction * factor
        predictions.append(prediction)
    addValueColumn(output_path, stories, predictions)
    return

def train(absolute_path):
    stories = getVectorFromFile(absolute_path,columnsToInclude=2)
    filtered = stop_and_stem([row[0] for row in stories])
    expected = [row[1] for row in stories]
    smvector = []
    for i in range(len(expected)):
        expected = expected / tf.calculate(filtered[i])
    for i in filtered:
        smvector.append(sm.map(filtered[i]))
    neuralnet.train(filtered,expected)
    neuralnet.saveWeightsToFile()
    return


if __name__ == '__main__':
    #TODO: Launch Neural Net
    app.run(debug=True,host='0.0.0.0',port=80)
    while running:
        server.handle_request()
