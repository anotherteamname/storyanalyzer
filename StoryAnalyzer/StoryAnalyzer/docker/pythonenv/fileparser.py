﻿"""
Author: Jacob Vines
Date: 4/1/2019

Purpose: Contains methods for fileparsing
"""

import csv

'''
    Takes parameter "columnsToInclude" to indicate whether the return vector
    should be formed of the first column of the source file or the first and second
    column of the source. Defaults to 1, as it will be called the most.
'''
def getVectorFromFile(filename, columnsToInclude=1):
    if columnsToInclude > 2 or columnsToInclude < 0:
        return []
    return_vector = []
    with open(filename, 'r', encoding="utf-8") as inputfile:
        csv_reader = csv.reader(inputfile)
        for row in csv_reader:
            if columnsToInclude == 1:
                return_vector.append(row[0])
            elif columnsToInclude == 2:
                return_vector.append(row)
    return return_vector

'''
    Appends columnToAdd to OriginalUserStories and outputs into a file at filename.
'''
def addValueColumn(filename, originalUserStories, columnToAdd):
    with open(filename, 'w', encoding="utf-8",newline='') as outputfile:
        csv_writer = csv.writer(outputfile)
        for i in range(len(originalUserStories)):
            csv_writer.writerow([originalUserStories[i],columnToAdd[i]])
    return

def calculateAccuracy_ToCSV(filename,predictions,actuals):
    errorsum = 0.0
    num_correct = 0
    for i in range(len(predictions)):
        errorsum += abs((actuals[i] - predictions[i])/actuals[i])
        if actuals[i] == predictions[i]:
            num_correct += 1
    mape = errorsum / len(predictions)
    accuracy = num_correct/len(predictions)
    with open(filename,'a',encoding="utf-8",newline='') as file:
        file.write("Accuracy,"+str(accuracy)+"\n"+"MAPE,"+str(mape))
    return mape,accuracy