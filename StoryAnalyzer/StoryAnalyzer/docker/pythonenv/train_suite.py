from fileparser import getVectorFromFile,addValueColumn,calculateAccuracy_ToCSV
from neuralnet import Perceptron
from techfactorsmanager import Techfactors
from matrixmanager import Sparsematrix
from nlpmanager import stop_and_stem
from datetime import datetime

it=1
lr=15

def train(filename="Training.csv"):
    tf = Techfactors()
    sm = Sparsematrix()
    nn = Perceptron(len(sm.dictionary),learning_rate=lr,iterations=it,buildFromFile=False)
    print("______ TRAIN SCRIPT ______")
    print("Getting stories from training set...")
    stories = getVectorFromFile("./testing_sets/" + filename,columnsToInclude=2)
    print("Stopping and stemming...")
    filtered = stop_and_stem([row[0] for row in stories])
    expected = [int(row[1]) for row in stories]
    print("Calculating tech factor scalars for stories...")
    smvector = []
    #for i in range(len(expected)):
    #    expected[i] = expected[i] / float(tf.calculate(filtered[i]))
    print("Forming sparse matrices for each user story. This may take a while...")
    for row in filtered:
        mapping = sm.map(row)
        smvector.append(mapping)
    print("Training on dataset. This may take a while...")
    nn.train(smvector,expected)
    print("Done training...")
    nn.saveWeightsToFile(ascsv=True)
    print("Weights saved to numpy file: nnweights.npy")
    return

def test(filename="Testing.csv"):
    tf = Techfactors()
    sm = Sparsematrix()
    nn = Perceptron(len(sm.dictionary),learning_rate=lr,iterations=it)
    print("______ TEST SCRIPT ______")
    print("Getting stories from testing set...")
    stories = getVectorFromFile("./testing_sets/" + filename,columnsToInclude=2)
    sto = [row[0] for row in stories]
    filtered = stop_and_stem(sto)
    expected = [int(row[1]) for row in stories]
    print("Testing on each user story. This may take a while...")
    predictions = []
    for row in filtered:
        factor = tf.calculate(row)
        matrix = sm.map(row)
        prediction = nn.activate(matrix)
        prediction = prediction * factor
        predictions.append(prediction)
    day = datetime.now().strftime('date%m-%d-time%H.%M')
    filename = "./testing_sets/results/training_lr" + str(nn.learning_rate) + "iter" + str(nn.iterations) + day +".csv"
    addValueColumn(filename,sto,predictions)
    print("Done training...")
    print("Calculating Accuracy and outputting to file")
    mape,acc = calculateAccuracy_ToCSV(filename,predictions,expected)
    print("Testing output written to " + filename)
    print("Accuracy = " + str(acc))
    print("MAPE =" + str(mape))
    