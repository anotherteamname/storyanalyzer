﻿// ---------------------------------------------------------------
// File Name: HelpWindow.xaml.cs
// Project Name: Story Analyzer
// ---------------------------------------------------------------
// Purpose: Load and display a help dialog
// ---------------------------------------------------------------

using System.Windows;

namespace StoryAnalyzer
{
	/// <summary>
	/// Interaction logic for HelpWindow.xaml
	/// </summary>
	public partial class HelpWindow : Window
	{
		private string HELP_FILE_CONTENTS = StoryAnalyzer.Properties.Resources.help;//"..\\..\\Data\\help.txt";

		public HelpWindow()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Load the contents of the help file when the window loads.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TxtHelpDisplay_Loaded(object sender, RoutedEventArgs e)
		{
			this.txtHelpDisplay.Text = this.HELP_FILE_CONTENTS;

		}
	}
}
