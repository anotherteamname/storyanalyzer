﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace StoryAnalyzer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string InputFileName;
        private string OutputFileName;
        private string TechFactorsCSV = Directory.GetCurrentDirectory() + @"/Data/Default_Tech_Factors.csv";
        private bool defaultChanged = false;
        private Stopwatch stopWatch; // Time the program's execution
		private Process cmdProcess; // The cmd prompt's process so that it can be closed at the end

        // This port is also hard-coded in Python and Docker files
        const string SERVER_NAME = @"http://localhost";
        int SERVER_PORT = 8000;

        public MainWindow()
        {
			StartDocker();
			Thread.Sleep(4000);
            InitializeComponent();
			stopWatch = new Stopwatch();
			InputFileName = String.Empty;
			OutputFileName = String.Empty;
        }


		/// <summary>
		/// Setup the connection with the docker container once the window is loaded
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
		}


		/// <summary>
		/// Event Handler for when the exit menu item is clicked, where it shall exit out of the program
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ExitItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        /// <summary>
        /// Event hanlder for when the change input button is clicked, opens up a file dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeInputButton_Click(object sender, RoutedEventArgs e)
        {
			InputFileName = IO.GetOpenFileName( );
			InputBox.Text = InputFileName;
        }

        /// <summary>
        /// Event Handler for when the change output button is clicked, opens up a save dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeOutputButton_Click(object sender, RoutedEventArgs e)
        {
			OutputFileName = IO.GetSaveFileName( );
			OutputBox.Text = OutputFileName;
        }

		/// <summary>
		/// Event Handler for when the analyze button is clicked, runs the python script in the docker container hopefully
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void AnalyzeButton_Click(object sender, RoutedEventArgs e)
		{
			lblStatus.Content = "Running...";                       // Change the status to running and begin timing
			lblStatus.Refresh();									// Force the label to update
			Thread.Sleep(50);										// Actually give the label time to update
			this.stopWatch = Stopwatch.StartNew();                  // Start timing the program's execution

			CommunicateWithDocker(sender, e);

			this.stopWatch.Stop();                                  // End the stopwatch's exectution
			lblStatus.Content = "Completed";						// Change the status to completed
			double elapsedTime = this.stopWatch.ElapsedMilliseconds;// Get the elapsed time
			double timeInSeconds = elapsedTime / 1000;				// Convert the time to seconds
			lblLastTime.Content = String.Format("Last job completed in {0:N2} seconds", timeInSeconds); //" + timeInSeconds.ToString("d2") + " seconds";
			lblLastTime.Visibility = Visibility.Visible;			// Show the time
		}

		/// <summary>
		/// Event Handler for clicking the Help menu item, opens the help window.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HelpItem_Click(object sender, RoutedEventArgs e)
		{
			HelpWindow helpWindow = new HelpWindow();
			helpWindow.ShowDialog();
		}


		/// <summary>
		/// Setup and communicate with a docker container
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CommunicateWithDocker(object sender, RoutedEventArgs e)
		{
			CheckForFileLocations(sender, e); // Make sure input and output locations were selected
			CommunicateWithContainer();
		}


		/// <summary>
		/// Start up the docker container either as debugging or silent
		/// </summary>
		private void StartDocker()
		{
			// Start the Docker container from CMD line call - requires Docker to be running,
			//  will not fail if the container is already running.
			string strCmdText;
			SERVER_PORT = FindOpenPort();

			strCmdText = "/C cd";
			strCmdText += Properties.Resources.DockerPath;
			strCmdText += "  && docker-compose run -p " + SERVER_PORT + ":80 pythonservice";

			// Setup the command process
			Process cmd = new Process();
			cmd.StartInfo.FileName = "CMD.exe";
			cmd.StartInfo.Arguments = strCmdText;
			cmd.StartInfo.CreateNoWindow = false;
			cmd.StartInfo.RedirectStandardInput = false;
			cmd.StartInfo.RedirectStandardOutput = false;
			cmd.StartInfo.UseShellExecute = true;
			//cmd.StartInfo.WindowStyle = ProcessWindowStyle.Normal;      // Display the command prompt for debugging
			//cmd.StartInfo.WindowStyle = ProcessWindowStyle.Minimized; // "Hide" the command prompt, using Hidden doesn't work right
			cmd.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;		// Keeps the command prompt from displaying to the user

			cmd.Start(); // Start the command prompt process
			this.cmdProcess = cmd;
		}


		/// <summary>
		/// Check if a file location was selected for input and output
		/// </summary>
		/// <param name="sender">The information concerning the sender, i.e. the analyze click</param>
		/// <param name="e">Infomation concerning the environment at the time analyze click</param>
		private void CheckForFileLocations(object sender, RoutedEventArgs e)
		{
			if (this.InputFileName == String.Empty)
			{
				MessageBox.Show("No input file was selected. Please select an input file.", "No Input File", MessageBoxButton.OK, MessageBoxImage.Warning);
				ChangeInputButton_Click(sender, e);
			}
			if (this.OutputFileName == String.Empty)
			{
				MessageBox.Show("No output file was selected. Please select an output file.", "No Output File", MessageBoxButton.OK, MessageBoxImage.Warning);
				ChangeOutputButton_Click(sender, e);
			}
		}


		/// <summary>
		/// Once setup is complete, initiate and conduct contact with the docker container
		/// </summary>
		private void CommunicateWithContainer ( )
		{
			try
			{
				string filename = GetFileName(this.InputFileName); // Get the file name from a string with a path

				string response = String.Empty;
				Boolean success = false;

				/* --- The communication process is as such:
				 *          1) upload the file to the python server
				 *          2) give the server some time to do its thing
				 *          3) issue a GET request for the file. if error in receiving the file, wait a little longer and try again.
				 */

				try
				{
					bool isEstimationSet = IO.IsEstimation(InputFileName);

					using (WebClient client = new WebClient())
					{
						if (isEstimationSet)
						{
							client.UploadFile(SERVER_NAME + ":" + SERVER_PORT + "/predict", InputFileName);
						}
						else
						{
							client.UploadFile(SERVER_NAME + ":" + SERVER_PORT + "/train", InputFileName);
						}
                        if(defaultChanged)
                            client.UploadFile(SERVER_NAME + ":" + SERVER_PORT + "/factors", TechFactorsCSV);

						Thread.Sleep(4000); // Let's give the python app 4 seconds to do its thing.
						int errorCount = 0;
						while (!success)
						{
							try
							{
								client.DownloadFile(SERVER_NAME + ":" + SERVER_PORT + "/retrieve/" + filename, OutputFileName); // Use to dump returned file to a file.
								//response = client.DownloadString(SERVER_NAME + ":" + SERVER_PORT + "/" + filename); // Use to receive returned file as a string.
								success = true;
							}
							catch (WebException)
							{
								Thread.Sleep(2000);
								errorCount++;
								if (errorCount > 5)
									break;
							}
						}
					} // Uploading the selected file
					if (!success)
						throw new Exception("Communication error in receiving file from server.");
					// Console.Write(response); // If string, dump to Console.v
				}
				catch (System.IO.InvalidDataException)
				{
					MessageBox.Show("Error: The input csv needs to either be one or two columns, no mixed values.", 
						"Invalid Input File", MessageBoxButton.OK, MessageBoxImage.Error);
				}

			}
			catch (ArgumentException ex)
			{
				MessageBox.Show(ex.Message, "File Name could not be read.", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			// We can catch the Throw new exception here if we can think of something to do to handle it.
		}


		/// <summary>
		/// Get the file's name from its path using default file path separators
		/// </summary>
		/// <param name="filePath">The path to a file that is getting it's name extracted</param>
		/// <exception cref="ArgumentException">Thrown if an invalid file path is given to the program</exception>
		/// <returns>The name and extention of a file</returns>
		private string GetFileName(string filePath)
		{
			string filename = Path.GetFileName(filePath);
			return filename;
		}


		/// <summary>
		/// Finds the first currently open port on the host device.
		/// </summary>
		/// <returns>The next open port on the host device.</returns>
		private int FindOpenPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }

        /// <summary>
        /// A simple about box that contains information the project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutItem_Click(object sender, RoutedEventArgs e)
		{
			string MESSAGE = "The StoryAnalyzer program was created for CSCI 3350, Software Engineering 2, " +
				"in the Spring semester 2019. Its purpose is to make estimating the completion time of user " +
				"stories easier for new developers. The program is written in C#.NET version 4.7.2 and Python " +
				"version 3.7.1." + Environment.NewLine + Environment.NewLine + "The backing AI for this program" +
				" runs in a Python container. You may find the AI source code and training data at the following " +
				"BitBucket repository: https://bitbucket.org/anotherteamname/storyanalyzer" + Environment.NewLine + 
				Environment.NewLine + "Thanks for using out program! We hope you find it helpful.";

			MessageBox.Show(MESSAGE, "About Box", MessageBoxButton.OK, MessageBoxImage.Information);
		}


		/// <summary>
		/// Automatically close the command line process when the application closes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
            using (WebClient client = new WebClient())
            {
                client.UploadData(SERVER_NAME + ":" + SERVER_PORT + "/shutdown", new byte[] { });
            }
		}

        /// <summary>
        /// Checks if the menu item is checked, if not, opens a file dialog to set a new tech factors .csv file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomFactors_Click(object sender, RoutedEventArgs e)
        {
            if(CustomFactors.IsChecked == true)
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.InitialDirectory = Directory.GetCurrentDirectory();
                dlg.Filter = "Microsoft Excel Comma Separated Values File |*.csv";
                if (dlg.ShowDialog() ?? false)
                {
                    TechFactorsCSV = dlg.FileName;
                    defaultChanged = true;
                }
                else
                    CustomFactors.IsChecked = false;
            }
            else
            {
                string current = Directory.GetCurrentDirectory();
                if(File.Exists(Path.Combine(current, @"Data/Default_Tech_Factors.csv")))
                {
                    Console.Write("Found");
                    TechFactorsCSV = Path.Combine(current, @"Data/Default_Tech_Factors.csv");
                }
            }
        }

        /// <summary>
        /// Should open excel since it's the default process for opening csvs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditFactors_Click(object sender, RoutedEventArgs e)
        {
            if(TechFactorsCSV != string.Empty)
            { try
                {
                    Process.Start(TechFactorsCSV);
                    defaultChanged = true;
                }
                catch(System.ComponentModel.Win32Exception)
                {
                    MessageBox.Show("File was not valid, could not open");
                }
            }
            else
            {
                MessageBox.Show("CSV not found");
            }
        }
    }

    /// <summary>
    /// Extension class to force UI elements to update
    /// </summary>
    public static class UpdateExtention
	{
		/// <summary>
		/// Empty action to for the UI to update
		/// </summary>
		private static readonly Action EmptyDelegate = delegate () { };

		/// <summary>
		/// Method to refresh an element on the UI
		/// </summary>
		/// <param name="uiElement">The element being refrested.</param>
		public static void Refresh(this UIElement uiElement)
		{
			uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
		}
	}
}
