﻿// ---------------------------------------------------------------
// File Name: IO.cs
// Project Name: Story Analyzer
// ---------------------------------------------------------------
// Purpose: Handle the reading and writting of text files for the
//		program. To be accessed with the public OpenFile and 
//		SaveFile methods. The class currently handles exception
//		by display error messages.
// ---------------------------------------------------------------

using Microsoft.Win32;
using System;
using System.IO;

namespace StoryAnalyzer
{
	/// <summary>
	/// Handle reading and writting text files for the rest of the program.
	/// </summary>
	static class IO
	{
		/// <summary>
		/// Allow the user to select a file of user stories to load into the program
		/// </summary>
		/// <returns>A file's path or an empty string if the user cancels.</returns>
		public static string GetOpenFileName()
		{
			string fileName = ""; // A file name with a default value if the user cancels the input

			// Set up the open file dialog
			OpenFileDialog openFileDialog = new OpenFileDialog
			{
				CheckFileExists = true,
				CheckPathExists = true,
				DefaultExt = ".csv",
				Filter = "CSV (*.csv)|*.csv|Text Files (*.txt)|*.txt|All Files (*.*)|*.*",
				InitialDirectory = Environment.CurrentDirectory,
				Title = "Select a file of User Stories to open."
			};

			// Display the file dialog and get the result from the user
			Nullable<bool> userSelection = openFileDialog.ShowDialog();

			if (userSelection == true)
				fileName = openFileDialog.FileName;

			return fileName;
		}


		/// <summary>
		/// Allow the user to select a file of user stories to load into the program
		/// </summary>
		/// <returns>A file's path or an empty string if the user cancels.</returns>
		public static string GetSaveFileName()
		{
			string fileName = ""; // A file name with a default value if the user cancels the input

			// Set up the save file dialog
			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				AddExtension = true,
				CheckFileExists = false,
				CheckPathExists = true,
				DefaultExt = ".csv",
				Filter = "CSV (*.csv)|*.csv|Text Files (*.txt)|*.txt|All Files (*.*)|*.*",
				InitialDirectory = Environment.CurrentDirectory,
				OverwritePrompt = true,
				Title = "Select a file location to save the User Stories."
			};

			// Display the file dialog and get the result from the user
			Nullable<bool> userSelection = saveFileDialog.ShowDialog();

			if (userSelection == true)
				fileName = saveFileDialog.FileName;

			return fileName;
		}


		/// <summary>
		/// Check if the csv is a training of estimation set
		/// </summary>
		/// <param name="filePath">Path to a CSV file to check if it has one or two columns</param>
		/// <returns>True if the file is one column (estimation) and false if it is two columns (training)</returns>
		public static bool IsEstimation(string filePath)
		{
			return ParseCSV(filePath);
		}


		/// <summary>
		/// Parse the csv to make sure it is consistently one or two columns
		/// </summary>
		/// <param name="filePath">Path to a file that contains user stories and potentially results</param>
		/// <exception cref="InvalidDataException">Thrown if the csv isn't consistent in its columns</exception>
		/// <returns>True if the file is consistenly one column and false otherwise</returns>
		private static bool ParseCSV(string filePath)
		{
			bool isOneColumn; // Check if the file starts as either one or two columns
			bool isConsistent = true; // Check if the file remains consistent as either one or two columns

			using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(filePath))
			{
				parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
				parser.SetDelimiters(",");

				string[] fields = parser.ReadFields();
				isOneColumn = fields.Length == 1 ? true : false;

				while (!parser.EndOfData)
				{ // Process Rows

					fields = parser.ReadFields();
					if (isOneColumn)
					{
						if (fields.Length == 2)
						{
							isConsistent = false;
							break;
						}
					}
					else
					{
						if (fields.Length == 1)
						{
							isConsistent = false;
							break;
						}
					}

					if (fields.Length == 1 || fields.Length == 2) 
					{
						continue;
					}
					throw new InvalidDataException(); // The file is invalid, error.
				}
			}

			if (!isConsistent) // The file wasn't consistent so error
			{
				throw new InvalidDataException();
			}

			return isOneColumn;
		}
	}
}
